/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */

#include <stdio.h>
/* Definitions of physical drive number for each drive */
#define DEV_RAM		0	/* Example: Map Ramdisk to physical drive 0 */
#define DEV_MMC		1	/* Example: Map MMC/SD card to physical drive 1 */
#define DEV_USB		2	/* Example: Map USB MSD to physical drive 2 */

#define SECTOR_COUNT 				((2 * 1024 * 1024))
#define SECTOR_SIZE					512 //4096//256 //4096
#define BLOCK_SIZE 					1 //65535

static char pc_file_name[] = "test.img";
static FILE *fp = NULL;
/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	int result;

	// printf("disk_status, pdrv: %d\r\n", pdrv);

	switch (pdrv) {
	case DEV_RAM :
		// result = RAM_disk_status();

		// translate the reslut code here

		return RES_OK;

	case DEV_MMC :
		// result = MMC_disk_status();

		// translate the reslut code here

		return stat;

	case DEV_USB :
		// result = USB_disk_status();

		// translate the reslut code here

		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	int result;

	// printf("disk_initialize, pdrv: %d\r\n", pdrv);

	switch (pdrv) {
	case DEV_RAM :
		// result = RAM_disk_initialize();

		// translate the reslut code here
		if(fp == NULL) {
			fp = fopen(pc_file_name, "r+");
			if(fp == NULL) {
				return RES_NOTRDY;
			}
		}
		

		return RES_OK;

	case DEV_MMC :
		// result = MMC_disk_initialize();

		// translate the reslut code here

		return stat;

	case DEV_USB :
		// result = USB_disk_initialize();

		// translate the reslut code here

		return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	DRESULT res;
	int result;

	int read_len;

	// printf("disk_read, pdrv: %d\r\n", pdrv);

	switch (pdrv) {
	case DEV_RAM :
		// translate the arguments here

		// result = RAM_disk_read(buff, sector, count);

		// translate the reslut code here

		// printf("seek to %d\r\n", sector * SECTOR_SIZE);

		if(fseek(fp, sector * SECTOR_SIZE, SEEK_SET)) {
			printf("wirte seek failed\r\n");
			return RES_ERROR;
		}
		read_len = fread(buff, count * SECTOR_SIZE, 1, fp);
		if(1 != read_len) {
			printf("--read len not match: %d\r\n", read_len);
			// return RES_ERROR;
		}


		return RES_OK;

	case DEV_MMC :
		// translate the arguments here

		// result = MMC_disk_read(buff, sector, count);

		// translate the reslut code here

		return res;

	case DEV_USB :
		// translate the arguments here

		// result = USB_disk_read(buff, sector, count);

		// translate the reslut code here

		return res;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	DRESULT res;
	int result;

	int write_len;
	// int i = 0;

	// printf("disk_write, pdrv: %d, sector: %d, count: %d\r\n", pdrv, sector, count);

	switch (pdrv) {
	case DEV_RAM :
		// translate the arguments here

		// result = RAM_disk_write(buff, sector, count);

		// translate the reslut code here
		// printf("seek to %d\r\n", sector * SECTOR_SIZE);

		if(sector == 0) {
			printf("%s\n", buff);
		}

		if(fseek(fp, sector * SECTOR_SIZE, SEEK_SET)) {
			printf("wirte seek failed\r\n");
			return RES_ERROR;
		}
		write_len = fwrite(buff, count * SECTOR_SIZE, 1, fp);
		if(1 != write_len) {
			printf("--write len not match: %d\r\n", write_len);
			// return RES_ERROR;
		}
		// for(i = 0; i < count; i++) {
		// 	if(fseek(fp, (i + sector) * SECTOR_SIZE, SEEK_SET)) {
		// 		return RES_ERROR;
		// 	}
		// 	write_len = fwrite(buff, sector, 1, fp);
		// }

		return RES_OK;

	case DEV_MMC :
		// translate the arguments here

		// result = MMC_disk_write(buff, sector, count);

		// translate the reslut code here

		return res;

	case DEV_USB :
		// translate the arguments here

		// result = USB_disk_write(buff, sector, count);

		// translate the reslut code here

		return res;
	}

	return RES_PARERR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	int result;

	// printf("disk_ioctl, pdrv: %d, cmd: %d\r\n", pdrv, cmd);

	switch (pdrv) {
	case DEV_RAM :
		switch(cmd) {
			case CTRL_SYNC:
				// fclose(fp);
				return RES_OK;

			case GET_SECTOR_COUNT:
				*(DWORD*)buff = SECTOR_COUNT;
				return RES_OK;

			case GET_SECTOR_SIZE:
				*(DWORD*)buff = GET_SECTOR_SIZE;
				return RES_OK;

			case GET_BLOCK_SIZE:
				*(DWORD*)buff = GET_BLOCK_SIZE;
				return RES_OK;
		}

		// Process of the command for the RAM drive

		return res;

	case DEV_MMC :

		// Process of the command for the MMC/SD card

		return res;

	case DEV_USB :

		// Process of the command the USB drive

		return res;
	}

	return RES_PARERR;
}

