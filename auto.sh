#!/bin/bash
rm test.img
touch test.img
make -j$(nproc)
if [ $? -eq 0 ]; then
	echo success
	# hexdump -C test.img
	du -h test.img
fi