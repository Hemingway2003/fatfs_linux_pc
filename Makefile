TAGET = main.run

BUILD_DIR = build

CC = gcc
SIZE = size
COPY = cp
ECHO = echo


C_SOURCES = \
main.c \
fatfs/diskio.c \
fatfs/ff.c \
fatfs/ffsystem.c \
fatfs/ffunicode.c 

INCLUDES = \
-Ifatfs

################################################################
# $@ 目标文件
# $^ 全部依赖
# $< 第一个依赖
# $? 第一个变化的依赖

OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))

all: $(BUILD_DIR)/$(TAGET)
	@$(ECHO) "-------------------------------"
	@$(ECHO) "-------------------------------"
	@./$(TAGET)

$(BUILD_DIR)/$(TAGET): $(OBJECTS) Makefile
	$(CC) $(OBJECTS) $(INCLUDES) -o $@
	$(SIZE) $@
	@$(COPY) $@ .
	

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)
	$(CC) $(INCLUDES) -c $< -o $@


$(BUILD_DIR):
	mkdir $@

.PHONY: clean all

clean:
	@rm -rf $(BUILD_DIR)
	@rm $(TAGET)
